using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GemData 
{

    public int gems;

    public GemData (int gemNum){
        gems = gemNum;
    }
}
