using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GoldMultiplier 
{
    public int Count;

    public GoldMultiplier(int _count){
        Count = _count;
    }
}
