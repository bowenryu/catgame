// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleStoreKitTestTangle
    {
        private static byte[] data = System.Convert.FromBase64String("voSIhqWIj4uLiY2NvoOIh6QIxgiCht364fzrxef6v4W+h4iN24qInYzb3b+ZvpuIh6QIxgh5g4+Hj5iGnA1YCfbjFzPQPYRuFkB4ZE0A4+ei+2jCP5xdc22R0IPzNtZZLvJq8UonhT1B7IF1OjJ5VQrQpmXJg39jvtG/n76BiI3bio2Cht364fzrxecXdT0E2xFhk6Y20aRf/RG7ybk1yL+UHw43MWuCRbI1gMrsqHSKpzjHeYOPj4WLjo0Mj4+OPI5usn9mZ+y+DI36vgyM0i6NjI+MjI+PvoOIh2nhbpVog+ZDq+BN+IZ7xIUg9PqyTsmmxWXUT6WH2KIrJIeN9by/UmZB6rLjDte9AkS4iKSflQPHQIZx6Pq/n76BiI3bioSCht364fzrxef6k52Pj3GKi76Nj49xvoCIjduTgY+OvgyPhIwMj4+OVfEesqL9gVUZqGU5zg+zCbP+319K0wMrUvHk1pRQ3frh/OvF5/q+kJmDvL6+ur6/v7lZavYmISbjKU9U5qPoQkGnhd6ibcUdNYR2L6+wRu9XqkbQkfuwVB1VzyvwhwM0s7ND/tlWY2FbVPlGHYqsvoOIh6QIxgh5g4+Pj4uOjQyPgb+fvoGIjduKhYKG3frh/OvF5/q/BSJ7ET6Hti3g9UWo3oSUbwdf/PiKjYKG3frh/OvF5/q/n76BiI3bil34nnx+52JD7Bf/d+4ECsa2SjMFAm//VcTLfaOXGI3YDyjKYPv51d3u+LncYnEil9OCm6EEgXsHRlmlk6QIxgh5g4+PhYuOvtG/n76BiI3bhbfCRcHgTj3RORs0NybTxPolYeG7vL271JmDur6+vby5v7m7vL271ISCht364fzrxef6v5++gYiN24qFkmGHBvbrQA98A/xpiZTgqJx5vD9MdWjRnE+MjY+Ojy21vre+gYiN20H2ItM0sNHepdKnAlNMWaSZUaw6M05Yk84Q2Te2Hvn43Fwmxf7gNumFvoeIjduKiJ2M292/mb6biIekCMYIeYOPh4+Yht364fzrxef6vgyP7SUZFKVNpFZePG9b1IO3leHppifpqzcl4r3rI1ZMFPbGgWdCT6mDSIM1th7liEpyg2sb3kMfRjSretZHC/bGQE5LnJ/kgYIgoYtE4fTq8a6PcYqKjYyMCr6YiI3bk6uPj3GKgpBipR+LKbPB");
        private static int[] order = new int[] { 30,4,5,41,39,18,8,33,16,31,30,17,37,36,31,28,31,42,31,26,28,21,36,30,26,31,34,34,43,39,43,33,41,36,34,36,41,42,42,40,41,43,42,43,44 };
        private static int key = 142;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
