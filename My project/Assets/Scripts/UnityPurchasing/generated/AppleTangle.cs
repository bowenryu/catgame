// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("kKvKmb6FQ5RcEaG33sVWlFLmX1S8s7y2tKG8urv1lKChvbqnvKGs5OVX0W7lV9Z2ddbX1NfX1Nfl2NPcobyzvLa0obD1t6z1tLus9aW0p6HSOajsVl6G9QbtEWRqT5rfvir+KbJa3WH1Ih55+fW6pWPq1OVZYpYa2kjoJv6c/c8dKxtgbNsMi8kDHujR08bXgIbkxuXE09aA0d/G35SlpRW25qIi79L5gz4P2vTbD2+mzJpgYO94Idrb1UfeZPTD+6EA6dgOt8Pg5+Th5ebjj8LY5uDl5+Xs5+Th5X52pEeShoAUevqUZi0uNqUYM3aZ/1OdUyLY1NTQ0NXlt+Te5dzT1oBrIaZOOwex2h6smuENd+ssrSq+HfrlVBbT3f7T1NDQ0tfX5VRjz1Rm5cTT1oDR38bflKWlubD1nLu2++TT5drT1oDIxtTUKtHQ5dbU1CrlyN3+09TQ0NLX1MPLvaGhpabv+vqiylBWUM5M6JLiJ3xOlVv5AWRFxw3o87L1X+a/IthXGgs+dvoshr+OscpEDsuShT7QOIusUfg+43eCmYA5u7H1trq7sbyhvLq7pvW6s/WgprDT1oDI29HD0cH+BbySQaPcKyG+WN2L5VfUxNPWgMj10VfU3eVX1NHl8Tc+BGKlCtqQNPIfJLitODJgwsL1lpTlV9T35djT3P9TnVMi2NTU1FfU1dPc/1OdUyK2sdDU5VQn5f/TDOOqFFKADHJMbOeXLg0ApEurdIfD5cHT1oDR1sbYlKWlubD1h7q6obHg9sCewIzIZkEiI0lLGoVvFI2FoqL7tKWlubD7trq4+rSlpbmwtrT59bawp6G8s7y2tKGw9aW6uby2rFqmVLUTzo7c+kdnLZGdJbXtS8AgXsxcCyyeuSDSfvfl1z3N6y2F3AacDaNK5sGwdKJBHPjX1tTV1HZX1K/lV9Sj5dvT1oDI2tTUKtHR1tfUfQmr9+Af8AAM2gO+AXfx9sQidHmqlH1NLAQfs0nxvsQFdm4xzv8WyvPl8dPWgNHexsiUpaW5sPWWsKeh40yZ+K1iOFlOCSaiTiejB6LlmhShvbqnvKGs5MPlwdPWgNHWxtiUpfuVcyKSmKrdi+XK09aAyPbRzeXDubD1nLu2++Tz5fHT1oDR3sbIlKX1urP1ob2w9aG9sLv1tKWluby2tPW0u7H1trCnobyzvLa0oby6u/WlHMynIIjbAKqKTifw1m+AWpiI2CSs9bSmpqC4sKb1tLa2sKWhtLu2sLe5sPWmobS7sbSnsfWhsKe4pvW02NPc/1OdUyLY1NTQ0NXWV9TU1YlizmhGl/HH/xLayGOYSYu2HZ5VwtDV1lfU2tXlV9Tf11fU1NUxRHzch7C5vLS7trD1urv1ob28pvW2sKelubD1h7q6ofWWlOXLwtjl4+Xh5+bjj+W35N7l3NPWgNHTxteAhuTGZOWNOY/R51m9ZlrIC7CmKrKLsGlAS6/ZcZJejgHD4uYeEdqYG8G8BFXB/gW8kkGj3Cshvlj7lXMikpiqpbmw9Zawp6G8s7y2tKG8urv1lKCntLahvLaw9aahtKGwuLC7oab75Yxy0NypwpWDxMuhBmJe9u6SdgC6hX9fAA8xKQXc0uJloKD0");
        private static int[] order = new int[] { 25,11,25,39,51,21,22,25,57,23,25,24,24,50,37,23,29,35,49,56,23,43,30,33,53,50,34,58,53,33,36,44,55,33,58,53,54,53,50,48,43,44,48,44,45,56,56,48,48,59,53,53,54,57,58,59,59,57,59,59,60 };
        private static int key = 213;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
